# Developed With (at 04.2024)
> All using the latest install
* PHP
* MariaDB
* MUI
* React
* Typescript
* SCSS

# Install
* Set up a MariaDB
    * Create the DB in MariaDB
* Use the `.env.example` and make a `.env` file
    * set the created DB name to be `mysql` DB name in the app at `line 22`
    * Add in all the missing credentials for the MariaDB (currently commented out)
* run cmd commands
    * `composer install`
    * `npm i`
    * `php artisan key:generate`
    * `php artisan migrate`
    * `php artisan db:seed --class=UsersSeeder`
    * `php artisan db:seed --class=CourseSeeder`

# Run the app
* Make sure `php` server is running
* Activate both in the CMD
    * `php artisan serve`
    * `npm run dev`
* Navigate to: `http://localhost:8000/`
* Login with 
    * email: `user@app.com`
    * password `password`    