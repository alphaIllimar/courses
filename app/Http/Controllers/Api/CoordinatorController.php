<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Models\Coordinator;
use Validator;

class CoordinatorController extends Controller
{
    public function index()
    {
        return Coordinator::all();
    }

    public function create(Request $request) {

        $body = json_decode($request->getContent(), true);
        $rules = [
            'name' => 'required|max:100',
            'surname' => 'required|max:100',        
        ];

        $validator = Validator::make($body, $rules);
        if ($validator->passes()) {
            //TODO Handle your data
            $coordinator = Coordinator::create($validator->safe()->all());
            return $coordinator;
        } else {
            return response()->json([
                'message' => "Validation error",
                'errors' => $validator->errors()->all()
            ], 500);
        }
    }
}
