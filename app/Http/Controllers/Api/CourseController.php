<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

use App\Models\Course;
use App\Models\CourseCoordinator;

class CourseController extends Controller
{
    public function index()
    {
        $courses = Course::all();

        foreach($courses as $course) {
            foreach($course->coordinatedBy as $coordinator) {
                $coordinator->info;
            }
        }

        return $courses;
    }

    public function show(Course $course) {
        foreach($course->coordinatedBy as $coordinator) {
            $coordinator->info;
        }

        return $course;
    }

    public function create(Request $request) {
        $body = json_decode($request->getContent(), true);
        $rules = [
            'name' => 'required|max:200',
            'description' => 'max:2000',        
            'studyLoad' => 'required|integer',
            'level' => 'required|max:8',
            'startDate' => 'required|date_format:Y-m-d\TH:i:s.000\Z',
            'endDate' => 'required|date_format:Y-m-d\TH:i:s.000\Z',
            'courseLengthInDays' => 'required|integer',
            'coordinatedBy' => 'required|array'
        ];

        $validator = Validator::make($body, $rules);
        if ($validator->passes()) {

            $data = $validator->safe()->all();
            $course = Course::create($data);
            $course->coordinatedBy;
            
            foreach($data['coordinatedBy'] as $i => $id) {
                $coordinator = $course->coordinatedBy()->create([
                    'primary' => $i === 0,
                    'coordinator_id' => $id
                ]);
                $course->coordinatedBy->push($coordinator);
                $coordinator->info;
            }

            return $course;
        } else {
            return response()->json([
                'message' => "Validation error",
                'errors' => $validator->errors()->all()
            ], 500);
        }
    }

    public function edit(Course $course, Request $request) {

        $body = json_decode($request->getContent(), true);
        $rules = [
            'name' => 'required|max:200',
            'description' => 'max:2000',        
            'studyLoad' => 'required|integer',
            'level' => 'required|max:8',
            'startDate' => 'required|date_format:Y-m-d\TH:i:s.000\Z',
            'endDate' => 'required|date_format:Y-m-d\TH:i:s.000\Z',
            'courseLengthInDays' => 'required|integer',
            'coordinatedBy' => 'required|array'
        ];

        $validator = Validator::make($body, $rules);
        if ($validator->passes()) {      

            $data = $validator->safe()->all();
            $course->update($data);
            $course->coordinatedBy()->delete();            
            $course->coordinatedBy;        
            
            foreach($data['coordinatedBy'] as $i => $id) {
                $coordinator = $course->coordinatedBy()->create([
                    'primary' => $i === 0,
                    'coordinator_id' => $id
                ]);
                $course->coordinatedBy->push($coordinator);
                $coordinator->info;
            }

            return $course;
        } else {
            return response()->json([
                'message' => "Validation error",
                'errors' => $validator->errors()->all()
            ], 500);
        }
    }

    public function delete(Course $course) {
        $course->delete();

        return response()->json([
            'message' => 'Deleted'
        ]);
    }
}
