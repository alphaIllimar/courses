<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    function login(Request $request)
    {
        $requestUser = json_decode($request->getContent());
        $user= User::where('email', $requestUser->email)->first();        

        if (!$user || !Hash::check($requestUser->password, $user->password)) {
            return response([
                'message' => ['These credentials do not match our records.']
            ], 404);
        }

        $token = $user->createToken('API-TOKEN')->plainTextToken;
    
        $response = [
            'user' => $user,
            'token' => $token
        ];
    
        return response($response, 201);
    }

    public function logout(Request $request) {

        $request->user()->tokens()->delete();

        return response()->json([
            'message' => "Log-out"
        ], 200);
    }
}
