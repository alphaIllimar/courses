<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\CourseCoordinator;
use Illuminate\Support\Str;

class Course extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'studyLoad',
        'level',
        'startDate',
        'endDate',
        'courseLengthInDays',
    ];

    protected $casts = [
        'startDate' => 'datetime',
        'endDate' => 'datetime'
     ];

    public function coordinatedBy() {
        return $this->hasMany(CourseCoordinator::class);
    }

    public static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->courseId = Str::uuid();
        });
    }
}
