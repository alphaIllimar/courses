<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Coordinator;

class CourseCoordinator extends Model
{
    use HasFactory;

    protected $fillable = [
        'primary',
        'coordinator_id'
    ];

    public function info() {
        return $this->belongsTo(Coordinator::class, 'coordinator_id'); 
    }
}
