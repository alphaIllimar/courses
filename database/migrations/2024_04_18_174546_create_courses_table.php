<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->uuid('courseId');
            $table->string('name', 200);
            $table->string('description', 2000);
            $table->smallInteger('studyLoad');
            $table->string('level', 8);
            $table->timestampTz('startDate');
            $table->timestampTz('endDate');
            $table->smallInteger('courseLengthInDays');            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('courses');
    }
};
