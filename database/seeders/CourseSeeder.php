<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Models\Course;
use App\Models\CourseCoordinator;
use App\Models\Coordinator;

use Carbon\Carbon;

use DateTime;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void {
        
        $course = Course::create([
            'name' => 'Kursus #0',
            'description' => 'kirjeldus on suur',
            'studyLoad' => 10,
            'level' => 'bachelor',
            'startDate' => Carbon::create('2024-04-19T00:00:00.000'),
            'endDate' => Carbon::create('2024-04-29T00:00:00.000'),
            'courseLengthInDays' => 10
        ]);

        $info = Coordinator::create([
            'name' => 'Eesnimi',
            'surname' => 'Perekond'
        ]);

        $coordinator = $course->coordinatedBy()->create([
            'primary' => true,
            'coordinator_id' => $info->id
        ]);
    }
}
