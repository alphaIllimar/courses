import { PageLayout } from './Components/PageLayout/PageLayout';
import { Courses } from './Pages/Courses/Courses';
import { LogIn } from './Pages/LogIn/LogIn';
import { IState, useState } from './React-Hooks/useState';
import { g_service } from './Services/CoursesService/CoursesService';

export function App() {
    const _isLoggedIn:IState<boolean> = useState<boolean>(g_service.isLoggedIn());

    return (
        <PageLayout
            onLogOut={() => {
                _isLoggedIn.set(false);
            }}
        >
            { !_isLoggedIn.value ? (
                <LogIn 
                    onLogIn={() => {
                        _isLoggedIn.set(true);
                    }}
                />
            ) : <Courses /> }
        </PageLayout>
    );
}