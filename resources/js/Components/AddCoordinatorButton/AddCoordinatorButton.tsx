import { Add } from "@mui/icons-material";
import { Button, IconButton, Stack, TextField, Tooltip } from "@mui/material";
import { IAddCoordinatorButtonProps } from "./IAddCoordinatorButtonProps";
import { IState, useState } from "../../React-Hooks/useState";
import { DraggableDialog } from "../DraggableDialog/DraggableDialog";
import { ICoordinator } from "../../Models/ICoordinator";
import * as _ from 'lodash';
import { g_service } from "../../Services/CoursesService/CoursesService";


export function AddCoordinatorButton(props:IAddCoordinatorButtonProps) {
    const _coordinator:IState<ICoordinator|null> = useState<ICoordinator|null>(null);
    const _isSaving:IState<boolean> = useState<boolean>(false);

    return (
        <>
            <Tooltip title="Add coordinator">
                <IconButton
                    onClick={() => {
                        const coordinator:ICoordinator = {
                            id: 0,
                            name: '',
                            surname: ''
                        };
                        _coordinator.set(coordinator);
                    }}
                >
                    <Add />
                </IconButton>
            </Tooltip>
            <DraggableDialog
                isOpen={!!_coordinator.value}
                onClose={() => {
                    _coordinator.set(null);
                }}
                title="Add Coordinator"
                actions={(
                    <>
                        <Button 
                            variant="text"
                            onClick={() => {
                                _coordinator.set(null);
                            }}
                        >
                            Cancel
                        </Button>
                        <Button 
                            variant="contained"    
                            disabled={(
                                _isSaving.value
                                || (
                                    !_coordinator.value?.name.trim()
                                    || !_coordinator.value?.surname.trim()
                                )
                            )}                    
                            onClick={() => {
                                _isSaving.set(true);
                                g_service.addCoordinator(_coordinator.value as ICoordinator).then((result:ICoordinator|null) => {
                                    if(result) {
                                        props.onSave(result);
                                        _coordinator.set(null);
                                    }
                                    _isSaving.set(false);
                                }).catch((e) => {
                                    _isSaving.set(false);
                                });
                            }}
                        >
                            Save
                        </Button>
                    </>
                )}
            >
                <Stack 
                    spacing={1}
                    sx={{
                        paddingTop: '0.5em'
                    }}
                >
                    <TextField 
                        label="First Name" 
                        variant="outlined" 
                        fullWidth
                        value={_coordinator.value?.name || ''}
                        inputProps={{
                            maxLength: 100
                        }}
                        onChange={(e:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
                            const coordinator:ICoordinator = _.cloneDeep(_coordinator.value as ICoordinator);
                            coordinator.name = e.target.value;
                            _coordinator.set(coordinator);
                        }}
                    />
                    <TextField 
                        label="Surname" 
                        variant="outlined" 
                        fullWidth
                        value={_coordinator.value?.surname || ''}
                        inputProps={{
                            maxLength: 100
                        }}
                        onChange={(e:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
                            const coordinator:ICoordinator = _.cloneDeep(_coordinator.value as ICoordinator);
                            coordinator.surname = e.target.value;
                            _coordinator.set(coordinator);
                        }}
                    />
                </Stack>
            </DraggableDialog>
        </>
    );
}