import { ICoordinator } from "../../Models/ICoordinator";

export interface IAddCoordinatorButtonProps {
    onSave: (newCoordinator:ICoordinator) => void;
}