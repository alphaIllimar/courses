import { Box, Slider, Stack, Typography } from "@mui/material";
import { IAppSliderProps } from "./IAppSliderProps";

export function AppSlider(props:IAppSliderProps) {
    return (
        <Box>
            <Typography variant="body1">{props.label}</Typography>
            <Stack direction="row" spacing={1}>
                <Slider
                    max={props.max}
                    min={props.min}
                    value={props.value}
                    onChange={(e, newValue:number|number[]) => {
                        props.onChange(newValue as number);
                    }}
                    valueLabelDisplay="auto"
                    sx={{
                        flexGrow: 1
                    }}
                />
                <Typography variant="subtitle1">{props.value}</Typography>
            </Stack>
        </Box>
    );
}