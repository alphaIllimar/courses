export interface IAppSliderProps {
    max:number;
    min:number;
    label:string;
    value:number;
    onChange:(newValue:number) => void;
}