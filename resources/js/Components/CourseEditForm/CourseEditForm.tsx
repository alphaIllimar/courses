import { Box, Button, Chip, Drawer, FormControl, IconButton, InputLabel, MenuItem, Select, SelectChangeEvent, Stack, TextField, Tooltip, Typography } from "@mui/material";
import { ICourseEditFormProps } from "./ICourseEditFormProps";
import * as _ from 'lodash';
import { IState, useState } from "../../React-Hooks/useState";
import { ICourse, StudyLevel } from "../../Models/ICourse";
import * as React from 'react';
import { AppSlider } from "../AppSlider/AppSlider";
import { DatePicker } from "@mui/x-date-pickers";
import dayjs, { Dayjs } from "dayjs";
import { ICoordinator } from "../../Models/ICoordinator";
import { g_service } from "../../Services/CoursesService/CoursesService";
import { AddCoordinatorButton } from "../AddCoordinatorButton/AddCoordinatorButton";

export function CourseEditForm(props:ICourseEditFormProps) {
    const _course:IState<ICourse|null> = useState<ICourse|null>(_.cloneDeep(props.course));
    /**
     * Holds all the coordinators from the DB so that they do not need to be called every time
     */
    const _allCoordinators:IState<ICoordinator[]> = useState<ICoordinator[]>([]);
    const _isSaving:IState<boolean> = useState<boolean>(false);

    React.useEffect(() => {
        g_service.getAllCoordinators().then((coordinators:ICoordinator[]) => {
            _allCoordinators.set(coordinators);
        });
    }, []);
    
    React.useEffect(() => {
        _course.set(_.cloneDeep(props.course));
    }, [props.course]);

    const _getCourseLengthInDays = (course:ICourse|null):number => {
        const startDate:Dayjs = dayjs(course?.startDate || new Date()); 
        const endDate:Dayjs = dayjs(course?.endDate || new Date());
        const ret:number = endDate.diff(startDate, 'day') + 1;
        return ret < 0 ? 1 : ret;
    }

    const _courseLengthInDays:IState<number> = useState<number>(_getCourseLengthInDays(_course.value));

    React.useEffect(() => {
        _courseLengthInDays.set(_getCourseLengthInDays(_course.value));
    }, [_course.value]);
    
    const _setCourseEndDate = (course:ICourse, newLength?:number) => {        
        course.endDate = dayjs(course.startDate).add((newLength ? newLength : _getCourseLengthInDays(_course.value)) - 1, 'day').toDate();        
    };

    const _getCoordinatorName = (id:number):string => {
        const coordinator:ICoordinator|undefined = _allCoordinators.value.find((c:ICoordinator) => c.id === id);
        return `${coordinator?.name} ${coordinator?.surname}`;
    }

    /**
     * Re-orders coordinator ids array so that the primatry would be on top
     * @param ids The IDs to sorted
     * @param primary The new primary ID
     */
    const _reOrderCoordinators = (ids:number[], primary?:number):number[] => {
        if(primary) {
            ids = ids.filter((id:number) => id !== primary);
            ids.unshift(primary);
        }
        return ids;
    };

    return (
        <Drawer
            anchor={'right'}
            open={!!_course.value}
            PaperProps={{
                sx: { 
                    width: {
                        xs: '90%',
                        sm: "65%",
                        md: '40%'
                    },
                },
            }}
            onClose={() => {
                _course.set(null);
            }}
        >
            <Stack 
                spacing={2}
                sx={{
                    padding: '0.5em'
                }}
            >
                <Typography variant="h6">{props.title}</Typography>
                <TextField 
                    label="Name" 
                    variant="outlined" 
                    fullWidth
                    value={_course.value?.name}
                    inputProps={{
                        maxLength: 200
                    }}
                    onChange={(e:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
                        const course:ICourse = _.cloneDeep(_course.value as ICourse);
                        course.name = e.target.value;
                        _course.set(course);
                    }}
                    required
                />
                <TextField 
                    label="Description" 
                    variant="outlined" 
                    fullWidth
                    multiline
                    rows={4}
                    value={_course.value?.description}
                    inputProps={{
                        maxLength: 2000
                    }}
                    onChange={(e:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
                        const course:ICourse = _.cloneDeep(_course.value as ICourse);
                        course.description = e.target.value;
                        _course.set(course);
                    }}
                />
                <AppSlider
                    max={30}
                    min={1}
                    label="Study Load (ECT)"
                    value={_course.value?.studyLoad || 1}
                    onChange={(newValue:number) => {
                        const course:ICourse = _.cloneDeep(_course.value as ICourse);
                        course.studyLoad = newValue;
                        _course.set(course);
                    }}
                />
                <FormControl fullWidth>
                    <InputLabel>Level</InputLabel>
                    <Select                        
                        value={_course.value?.level || 'bachelor'}
                        label="Level"
                        onChange={(e:SelectChangeEvent) => {
                            const course:ICourse = _.cloneDeep(_course.value as ICourse);
                            course.level = e.target.value as StudyLevel;
                            _course.set(course);
                        }}
                    >
                        <MenuItem value={'bachelor'}>Bachelor</MenuItem>
                        <MenuItem value={'master'}>Master</MenuItem>
                        <MenuItem value={'doctoral'}>Doctoral</MenuItem>
                    </Select>
                </FormControl>
                <DatePicker 
                    minDate={dayjs(new Date())}
                    value={dayjs(_course?.value?.startDate || new Date())}
                    onChange={(date:Dayjs|null) => {
                        const course:ICourse = _.cloneDeep(_course.value as ICourse);
                        course.startDate = date?.toDate() || new Date();
                        _setCourseEndDate(course);
                        _course.set(course);
                    }}
                    label="Start Date"
                />
                <Box>
                    <AppSlider
                        max={365}
                        min={1}
                        label="Course Length"
                        value={_courseLengthInDays.value}
                        onChange={(newValue:number) => {                        
                            const course:ICourse = _.cloneDeep(_course.value as ICourse);
                            _setCourseEndDate(course, newValue);
                            _course.set(course);
                        }}
                    />
                    <Typography 
                        variant="body2"    
                        sx={{
                            textAlign: 'right',
                        }}
                    >
                        { dayjs(_course.value?.endDate || new Date()).format('DD/MM/YYYY') }
                    </Typography>
                </Box>
                <Stack direction='row' spacing={1}>
                    <FormControl 
                        fullWidth
                        sx={{
                            flexGrow: 1
                        }}
                    >
                        <InputLabel
                            required
                        >Coordinators</InputLabel>
                        <Select                        
                            value={(_course.value?.coordinatedBy.map((c:ICoordinator) => c.id) || []) as any}
                            multiple
                            label="Coordinators"
                            onChange={(e:SelectChangeEvent) => {
                                const course:ICourse = _.cloneDeep(_course.value as ICourse);
                                let coordinatorIds:number[] = e.target.value as any;
                                coordinatorIds = _reOrderCoordinators(coordinatorIds, _course.value?.coordinatedBy[0]?.id);                                
                                course.coordinatedBy = coordinatorIds.map((id:number) => _allCoordinators.value.find((c:ICoordinator) => c.id === id) as any);
                                _course.set(course);
                            }}
                            renderValue={(selection:any) => (selection as number[]).map((id) => _getCoordinatorName(id)).join(', ')}
                            required
                        >
                            {
                                _allCoordinators.value.map((coordinator:ICoordinator) => (
                                    <MenuItem value={coordinator.id} key={coordinator.id}>{coordinator.name} {coordinator.surname}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>
                    <AddCoordinatorButton
                        onSave={(newCoordinator:ICoordinator) => {

                            //adds the new coordinator to the saved coordinators list
                            const coordinators:ICoordinator[] = _.cloneDeep(_allCoordinators.value);                            
                            coordinators.push(newCoordinator);
                            _allCoordinators.set(coordinators);

                            //adds the new coordinator to the coordinators list
                            const course:ICourse = _.cloneDeep(_course.value as ICourse);
                            let coordinatorIds:number[] = (_course.value?.coordinatedBy.map((c:ICoordinator) => c.id) || []);
                            coordinatorIds.push(newCoordinator.id);
                            coordinatorIds = _reOrderCoordinators(coordinatorIds, _course.value?.coordinatedBy[0]?.id);                                
                            course.coordinatedBy = coordinatorIds.map((id:number) => coordinators.find((c:ICoordinator) => c.id === id) as any);
                            _course.set(course);
                        }}
                    />
                </Stack>
                <FormControl fullWidth>
                    <InputLabel>Primary Coordinator</InputLabel>
                    <Select                        
                        value={_course.value?.coordinatedBy[0]?.id as any || ''}
                        label="Primary Coordinator"
                        onChange={(e:SelectChangeEvent) => {
                            const course:ICourse = _.cloneDeep(_course.value as ICourse);
                            const newPrimaryCoordinator:number = e.target.value as any;
                            let coordinatorIds:number[] = (_course.value?.coordinatedBy || []).map((c:ICoordinator) => c.id);
                            coordinatorIds = _reOrderCoordinators(coordinatorIds, newPrimaryCoordinator);                                
                            course.coordinatedBy = coordinatorIds.map((id:number) => _allCoordinators.value.find((c:ICoordinator) => c.id === id) as any);                                
                            _course.set(course);
                        }}
                    >
                        {
                            (_course.value?.coordinatedBy || []).map((coordinator:ICoordinator) => (
                                <MenuItem value={coordinator.id} key={coordinator.id}>{coordinator.name} {coordinator.surname}</MenuItem>
                            ))
                        }
                    </Select>
                </FormControl>
                <Stack 
                    direction="row"
                    spacing={1}
                    sx={{
                        justifyContent: 'flex-end'
                    }}
                >
                    <Button 
                        variant="text"
                        onClick={() => {
                            _course.set(null);
                        }}
                    >
                        Cancel
                    </Button>
                    <Button 
                        variant="contained"          
                        disabled={(
                            _isSaving.value
                            || (
                                !_course.value?.name.trim()
                                || !_course.value?.coordinatedBy.length
                            )
                        )}              
                        onClick={() => {
                            _isSaving.set(true);                            
                            props.onSave(_.cloneDeep(_course.value as ICourse)).then(() => {
                                _isSaving.set(false);
                            });
                        }}
                    >
                        Save
                    </Button>
                </Stack>
            </Stack>
        </Drawer>
    )
}