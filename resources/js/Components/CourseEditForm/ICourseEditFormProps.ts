import { ICourse } from "../../Models/ICourse";

export interface ICourseEditFormProps {
    course:ICourse|null;
    onSave:(course:ICourse) => Promise<void>;
    title:string;
}