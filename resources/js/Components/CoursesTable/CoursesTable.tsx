import { DataGrid, GridActionsCellItem, GridColDef, GridRenderCellParams, GridRowParams } from "@mui/x-data-grid";
import { ICourse } from "../../Models/ICourse";
import { ICoursesTableProps } from "./ICoursesTableProps";
import { Visibility, Delete, Edit } from "@mui/icons-material";
import { Tooltip, Typography } from "@mui/material";
import { ICoordinator } from "../../Models/ICoordinator";

export function CoursesTable(props:ICoursesTableProps) {

    const _columns:GridColDef[] = [
        {
            field: 'actions',
            type: 'actions',
            width: 85,
            getActions: (params:GridRowParams<ICourse>) => [
                <GridActionsCellItem
                    icon={
                        <Tooltip title="View/Edit">
                            <Edit 
                                color='primary'                                
                            />
                        </Tooltip>
                    }
                    label={"View/Edit"}                    
                    onClick={() => {
                        props.onEdit(params.row);
                    }}                     
                />,                
                <GridActionsCellItem
                    icon={
                        <Tooltip title="Delete">
                            <Delete 
                                color="error"
                            />
                        </Tooltip>
                    }
                    label="Delete"                    
                    onClick={() => {                        
                        props.onDelete(params.row);
                    }}                     
                />
            ]
        },
        {
            field: 'name',
            headerName: 'Name',
            type: 'string',
            flex: 1,
            minWidth: 150
        },
        {
            field: 'description',
            headerName: 'Description',
            type: 'string',
            flex: 1,
            minWidth: 150
        },
        {
            field: 'courseId',
            headerName: 'Coordinator',
            type: 'string',
            flex: 1,
            minWidth: 150,
            renderCell: (params:GridRenderCellParams<ICourse>) => {                
                const coordinator:ICoordinator = params.row.coordinatedBy[0];
                return `${ coordinator?.name} ${ coordinator?.surname }`;
            }
        },
        {
            field: 'studyLoad',
            headerName: 'Study Load',
            type: 'number',
            flex: 1,
            minWidth: 150
        },
        {
            field: 'level',
            headerName: 'Level',
            type: 'string',
            flex: 1,
            minWidth: 150
        },
        {
            field: 'startDate',
            headerName: 'Start',
            type: 'date',
            flex: 1,
            minWidth: 150
        },
        {
            field: 'endDate',
            headerName: 'End',
            type: 'date',
            flex: 1,
            minWidth: 150
        },
        
    ];

    return (
        <DataGrid 
            rows={props.courses}
            columns={_columns}
            getRowId={(course:ICourse) => course.courseId}
            rowSpacingType={'margin'}
            getRowSpacing={(params) => ({
                top: 5,
                bottom: 5
            })}            
            pageSizeOptions={[5, 10, 25]}
            initialState={{
                pagination: { 
                    paginationModel: { 
                        pageSize: 10 
                    } 
                },
                sorting: {
                    sortModel: [{ 
                        field: 'id', 
                        sort: 'asc' 
                    }],
                },
            }}
            pagination
        />
    );
}