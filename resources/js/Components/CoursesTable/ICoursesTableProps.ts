import { ICourse } from "../../Models/ICourse";

export interface ICoursesTableProps {
    courses:ICourse[];
    onEdit:(course:ICourse) => void;
    onDelete:(course:ICourse) => void;
}