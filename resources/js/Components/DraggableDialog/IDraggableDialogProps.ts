export interface IDraggableDialogProps {
    /**
     * If TRUE the dialog will be opened
     */
    isOpen:boolean;
    /**
     * The method that will be called when the dialog will be closed
     */
    onClose():void;
    /**
     * The title of the dialog
     */
    title?:React.ReactNode;
    /**
     * The content of the dialog. If "string" then wrapped inside an DialogContentText tag.
     * @remarks Is overwritten by the children of the component
     */
    content?:React.ReactNode;
    /**
     * The actions i.e. buttons of the dialog
     */
    actions?:React.ReactNode;
}