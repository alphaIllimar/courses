
import { Box, Button, Divider, Paper, Stack, Typography } from '@mui/material';
import * as React from 'react';
import styles from './PageLayout.module.scss';
import { g_service } from '../../Services/CoursesService/CoursesService';
import { IPageLayoutProps } from './IPageLayoutProps';

export function PageLayout(props:React.PropsWithChildren<IPageLayoutProps>) {
    return (
        <Box>
            <Paper
                className={styles.container}
            >
                <Stack spacing={1}>                    
                    <Stack direction={'row'}>
                        <Typography 
                            variant='h6'
                            sx={{
                                flexGrow: 1
                            }}
                        >
                            Courses
                        </Typography>
                        {
                            g_service.isLoggedIn() &&
                            <Button 
                                variant='contained'
                                onClick={() => {
                                    g_service.logout().then((success:boolean) => {
                                        if(success) {
                                            props.onLogOut();
                                        }
                                    })
                                }}
                            >
                                Log-Out
                            </Button>
                        }
                    </Stack>                
                    <Divider />
                    <Box>
                        {props.children}
                    </Box>
                </Stack>                
            </Paper>
        </Box>
    );
}