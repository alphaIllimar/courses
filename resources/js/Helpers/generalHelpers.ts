/**
 * Returns an array of all the enum values in question
 * @param enumToBeParsed The enum that is to be parsed into value (Works only with numberic enums!)
 */
export function g_getEnumValues<T>(enumToBeParsed:any):T[] {
    const ret:T[] = Object.values(enumToBeParsed as any).filter(v => !isNaN(Number(v))) as T[];
    return ret;;
}
/**
 * Returns a local date from a ISO date i.e. where the iso date matches the local date
 * @param isoDate The ISO date string to be parsed
 */
export function g_getLocalDateFromIsoString(isoDate:string):Date {
    const date = new Date(isoDate);
    return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate());    
}
/**
 * The response type for the getNameOf function
 */
type GetNameOfResponseObjet<T> = {
    [key in keyof T]: string
}
/**
 * Gets the name of the object member as a string
 * @param obj The object whom member you seek
 * @param expression The method that will select the member from the object
 * @returns The name of the member as a string
 */
export function g_getNameOf<T>(obj:T, expression:(x:GetNameOfResponseObjet<T>) => string):string {    
    
    let response:GetNameOfResponseObjet<T> = {} as GetNameOfResponseObjet<T>;
    Object.getOwnPropertyNames(obj).forEach((key: string) => {
        response[key as keyof T] = key;
    });
    
    return expression(response);
}
/**
 * Returns a ISO date where the date has been shifted to the ISO date
 * @param date the local date whom you want the ISO UTC equalent date string
 */
export function g_getIsoDateFromLocalDate(date:Date):string {
    const isoDate:Date = new Date(2024, 0, 1);
    isoDate.setUTCFullYear(date.getFullYear());
    isoDate.setMonth(date.getMonth());
    isoDate.setDate(date.getDate());
    return isoDate.toISOString();
}