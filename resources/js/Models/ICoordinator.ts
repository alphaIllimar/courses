export interface ICoordinator {
    name:string;
    surname:string;
    id:number;
}