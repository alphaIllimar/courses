import { ICoordinator } from "./ICoordinator";

export type StudyLevel = 'bachelor'|'master'|'doctoral';

export interface ICourse {
    id:number;
    courseId:string;
    name:string;
    description:string;
    studyLoad:number;
    level:StudyLevel;
    startDate:Date;
    endDate:Date;
    coordinatedBy:ICoordinator[];
}