import { Box, Button, Stack } from "@mui/material";
import { ICourse } from "../../Models/ICourse";
import { IState, useState } from "../../React-Hooks/useState";
import * as React from 'react';
import { g_service } from "../../Services/CoursesService/CoursesService";
import { CoursesTable } from "../../Components/CoursesTable/CoursesTable";
import { CourseEditForm } from "../../Components/CourseEditForm/CourseEditForm";
import * as _ from 'lodash';

export function Courses() {
    const _courses:IState<ICourse[]> = useState<ICourse[]>([]);
    const _editCourse:IState<ICourse|null> = useState<ICourse|null>(null);
    const _addCourse:IState<ICourse|null> = useState<ICourse|null>(null);

    React.useEffect(() => {
        g_service.allCourses().then((courses:ICourse[]) => {
            _courses.set(courses);
        });
    }, []);

    const _handleEdit = (course:ICourse) => {
        _editCourse.set(_.cloneDeep(course));
    };

    const _handleDelete = (course:ICourse) => {
        g_service.deleteCourse(course.id).then((success:boolean) => {
            if(success) {
                const courses:ICourse[] = _courses.value.filter((c:ICourse) => c.id !== course.id);
                _courses.set(courses);
            }
        });
    }

    const _handleCourseEdit = async (toBeSaved:ICourse) => {        
        const savedCourse:ICourse|null = await g_service.editCourse(toBeSaved);
        if(savedCourse) {
            const courses:ICourse[] = _.cloneDeep(_courses.value);
            const index:number = courses.findIndex((c:ICourse) => c.id === savedCourse.id);
            courses.splice(index, 1, savedCourse);
            _courses.set(courses);
            _editCourse.set(null);
        }
    };

    const _handleCourseAdd = async (toBeSaved:ICourse) => {        
        const savedCourse:ICourse|null = await g_service.addCourse(toBeSaved);
        if(savedCourse) {
            const courses:ICourse[] = _.cloneDeep(_courses.value);
            courses.push(savedCourse);
            _courses.set(courses);
            _addCourse.set(null);
        }
    };

    return (
        <Box>
            <Stack spacing={1}>
                <CoursesTable
                    courses={_courses.value}
                    onEdit={_handleEdit}
                    onDelete={_handleDelete}
                />
                <Box 
                    sx={{
                        textAlign: 'right'
                    }}
                >
                    <Button
                        variant="contained"
                        onClick={() => {
                            const startDate:Date = new Date();
                            const endDate:Date = new Date();
                            endDate.setDate(endDate.getDate() + 29); //+29 days i.e. course length 30 days i.e. 29 + today

                            const newCourse:ICourse = {
                                name: '',
                                description: '',
                                level: 'bachelor',
                                studyLoad: 10,
                                startDate,
                                endDate,
                                coordinatedBy: [],
                                id: 0,
                                courseId: ''
                            };

                            _addCourse.set(newCourse);
                        }}
                    >
                        Add
                    </Button>
                </Box>
            </Stack>
            <CourseEditForm
                course={_editCourse.value}
                title="Edit Course"
                onSave={_handleCourseEdit}
            />
            <CourseEditForm
                course={_addCourse.value}
                title="Add Course"
                onSave={_handleCourseAdd}
            />
        </Box>
    )
}