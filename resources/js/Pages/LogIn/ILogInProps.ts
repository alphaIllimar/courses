export interface ILogInProps {
    onLogIn: () => void;
}