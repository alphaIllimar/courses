import { Alert, Box, Button, Stack, TextField } from "@mui/material";
import { IState, useState } from "../../React-Hooks/useState";
import { g_service } from "../../Services/CoursesService/CoursesService";
import { ILogInProps } from "./ILogInProps";

export function LogIn(props:ILogInProps) {
    const _email:IState<string> = useState<string>('');
    const _password:IState<string> = useState<string>('');
    const _showError:IState<boolean> = useState<boolean>(false);

    return (
        <>
            <Stack
                spacing={1}
            >
                <TextField 
                    label="E-Mail" 
                    variant="outlined" 
                    fullWidth
                    value={_email.value}
                    inputProps={{
                        maxLength: 100
                    }}
                    onChange={(e:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
                        _email.set(e.target.value);
                    }}
                />
                <TextField 
                    label="Password" 
                    variant="outlined" 
                    type="password"
                    fullWidth
                    value={_password.value}
                    onChange={(e:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
                        _password.set(e.target.value);
                    }}
                    inputProps={{
                        maxLength: 100
                    }}
                />
                <Box
                    sx={{
                        textAlign: 'right'
                    }}
                >
                    <Button
                        variant="contained"
                        onClick={() => {
                            g_service.login(_email.value, _password.value).then((success:boolean) => {
                                _showError.set(!success);
                                if(success) {
                                    props.onLogIn();
                                }
                            });
                        }}
                    >
                        Log-In
                    </Button>
                </Box>
                {
                    _showError.value &&
                    <Alert severity="error">
                        Could not log-in
                    </Alert>
                }                
            </Stack>
        </>
    );
}