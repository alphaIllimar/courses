import { ICoursesService } from "./ICoursesService";
import axios from 'axios';
import { ILogIn } from "./ILogInResponse";
import { ICourse } from "../../Models/ICourse";
import { ICoordinatorResponse, ICourseResponse } from "./ICourseResponse";
import { g_getIsoDateFromLocalDate, g_getLocalDateFromIsoString } from "../../Helpers/generalHelpers";
import { ICoordinator } from "../../Models/ICoordinator";
import * as _ from 'lodash';
import { ICoursePayload } from "./ICoursePayload";
import dayjs, { Dayjs } from "dayjs";

const ACCESS_TOKEN_KEY:string = 'Courses-Access-Token';

let COORDINATORS:ICoordinator[]|null = null;

export class CoursesService implements ICoursesService 
{
    private _getDataHeader() {
        return {
            Accept: 'application/json',
            Authorization: `Bearer ${sessionStorage.getItem(ACCESS_TOKEN_KEY)}`
        };
    }

    public async login(email:string, password:string):Promise<boolean> {
        try {
            const data:ILogIn = (await axios.post('/api/user/login', {
                email,
                password
            }, {
                headers: {
                    Accept: 'application/json'
                }
            })).data;
    
            sessionStorage.setItem(ACCESS_TOKEN_KEY, data.token);
            return true;
        } catch(e) {
            return false;
        }
    }

    public async logout():Promise<boolean> {
        try {
            await axios.get('/api/user/logout', {
                headers: this._getDataHeader()
            });
            sessionStorage.removeItem(ACCESS_TOKEN_KEY);
            return true;
        } catch(e) {
            return false;
        }
    }

    public isLoggedIn():boolean {
        return !!sessionStorage.getItem(ACCESS_TOKEN_KEY);
    }

    private _parseCourseResponse(data:ICourseResponse):ICourse {

        const course:ICourse = {
            id: data.id,
            courseId: data.courseId,
            name: data.name,
            description: data.description,
            studyLoad: data.studyLoad,
            level: data.level,
            startDate: g_getLocalDateFromIsoString(data.startDate),
            endDate: g_getLocalDateFromIsoString(data.endDate),
            coordinatedBy: []
        };

        data.coordinated_by.filter((c:ICoordinatorResponse) => !!c.primary).forEach((c:ICoordinatorResponse) => {
            course.coordinatedBy.push(c.info);
        });
        data.coordinated_by.filter((c:ICoordinatorResponse) => !c.primary).forEach((c:ICoordinatorResponse) => {
            course.coordinatedBy.push(c.info);
        });

        return course;
    }

    public async allCourses():Promise<ICourse[]> {
        try {
            const response:ICourseResponse[] = (await axios.get('/api/course', {
                headers: this._getDataHeader()
            })).data;

            const ret:ICourse[] = response.map((data:ICourseResponse) => this._parseCourseResponse(data));
            return ret;
        } catch(e) {
            return [];
        }
    }

    public async deleteCourse(id:number):Promise<boolean> {
        try {
            await axios.delete(`/api/course/${id}`, {
                headers: this._getDataHeader()
            })
            return true;
        } catch(e) {
            return false;
        }
    }

    public async getAllCoordinators():Promise<ICoordinator[]> {
        try {
            if(!COORDINATORS) {
                COORDINATORS = (await axios.get('/api/coordinator', {
                    headers: this._getDataHeader()
                })).data;
            }
            return _.cloneDeep(COORDINATORS as ICoordinator[]);
        } catch(e) {
            return [];
        }
    }

    public async addCoordinator(coordinator:ICoordinator):Promise<ICoordinator|null> {
        try {
            const newCoordinator:ICoordinator = (await axios.post('/api/coordinator', coordinator, {
                headers: this._getDataHeader()
            })).data;

            COORDINATORS?.push(_.cloneDeep(newCoordinator));

            return newCoordinator;
        } catch(e) {
            return null;
        }
    }

    private _parseCourseToPayload(data:ICourse):ICoursePayload {
        const startDate:Dayjs = dayjs(data.startDate);
        const endDate:Dayjs = dayjs(data.endDate);

        return {
            name: data.name,
            description: data.description,
            studyLoad: data.studyLoad,
            level: data.level,
            startDate: g_getIsoDateFromLocalDate(data.startDate),
            endDate: g_getIsoDateFromLocalDate(data.endDate),
            coordinatedBy: data.coordinatedBy.map((c:ICoordinator) => c.id),
            courseLengthInDays: endDate.diff(startDate, 'day') + 1
        };
    }

    public async editCourse(course:ICourse):Promise<ICourse|null> {
        try {
            const editCourse:ICourseResponse = (
                await axios.put(`/api/course/${course.id}`, 
                this._parseCourseToPayload(course), 
                {
                    headers: this._getDataHeader()
                })
            ).data;

            return this._parseCourseResponse(editCourse);
        } catch(e) {
            return null;
        }
    }

    public async addCourse(course:ICourse):Promise<ICourse|null> {
        try {
            const editCourse:ICourseResponse = (
                await axios.post(`/api/course`, 
                this._parseCourseToPayload(course), 
                {
                    headers: this._getDataHeader()
                })
            ).data;

            return this._parseCourseResponse(editCourse);
        } catch(e) {
            return null;
        }
    }
}

export const g_service:ICoursesService = new CoursesService();