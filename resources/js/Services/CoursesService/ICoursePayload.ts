import { StudyLevel } from "../../Models/ICourse";

export interface ICoursePayload {
    name:string;
    description:string;
    studyLoad:number;
    level:StudyLevel;
    startDate:string;
    endDate:string;
    courseLengthInDays:number;
    coordinatedBy:number[];
}