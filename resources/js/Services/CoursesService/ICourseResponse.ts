import { ICoordinator } from "../../Models/ICoordinator";
import { StudyLevel } from "../../Models/ICourse";

export interface ICourseResponse {
    id:number;
    courseId:string;
    name:string;
    description:string;
    studyLoad:number;
    level:StudyLevel;
    startDate:string;
    endDate:string;
    coordinated_by:ICoordinatorResponse[];
}

export interface ICoordinatorResponse {
    info:ICoordinator;
    primary:1|0;
}
