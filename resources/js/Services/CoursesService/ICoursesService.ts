import { ICoordinator } from "../../Models/ICoordinator";
import { ICourse } from "../../Models/ICourse";

export interface ICoursesService {
    /**
     * Loggs in the user
     * @param email the user e-mail
     * @param password The user password
     * @returns TRUE if successful and FALSE if not
     */
    login:(email:string, password:string) => Promise<boolean>;
    logout:() => Promise<boolean>;
    isLoggedIn:() => boolean;
    allCourses:() => Promise<ICourse[]>;
    deleteCourse:(id:number) => Promise<boolean>;
    getAllCoordinators:() => Promise<ICoordinator[]>;
    addCoordinator:(coordinator:ICoordinator) => Promise<ICoordinator|null>;
    editCourse:(course:ICourse) => Promise<ICourse|null>;
    addCourse:(course:ICourse) => Promise<ICourse|null>;
}