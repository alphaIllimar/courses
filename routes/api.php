<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\UserController;

use App\Http\Controllers\Api\CourseController;
use App\Http\Controllers\Api\CoordinatorController;

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:sanctum');


Route::post("/user/login", [UserController::class, 'login']);


Route::group(['middleware' => 'auth:sanctum'], function () {

    Route::get("/user/logout", [UserController::class, 'logout']);

    Route::get('/course', [CourseController::class, 'index']);
    Route::get('/course/{course}', [CourseController::class, 'show']);
    Route::post('/course', [CourseController::class, 'create']);
    Route::put('/course/{course}', [CourseController::class, 'edit']);
    Route::delete('/course/{course}', [CourseController::class, 'delete']);

    Route::get('/coordinator', [CoordinatorController::class, 'index']);
    Route::post('/coordinator', [CoordinatorController::class, 'create']);
});


